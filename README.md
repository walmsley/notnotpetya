```bash
npm install
npm run build -- css-themes
npm start
```

-   [ ] Context

    -   [ ] Historical

    -   [ ] Graph

-   [ ] Political

    -   [ ] Russia / Ukraine

    -   [ ] US

    -   [ ] Europe

-   [ ] Economic (Ismail)

    -   [ ] 10B $

    -   [ ] Maersk

    -   [ ] Not ransomware

    -   [ ] Cyberinsurance

-   [ ] Social (Elias)

    -   [ ] Easy to cause damage

    -   [ ] Critical infrastructure
        -   ATMs
        -   Metro system
        -   Hospitals
        -   Banks

-   [ ] Technical security (Kalan)

    -   [ ] Open source

    -   [ ] Connectivity

```mermaid
flowchart TB
    NSA>"NSA\nNational Security Agency"]
    USA{{"🏴 United States"}}
    ETB(["☠ Eternal Blue"])
    MIM(["☠ Mimikatz"])
    NOT(["☠ NotPetya"])
    RUS{{"🏴 Russia"}}
    SND>"Sandworm"]
    SHD>"Shadow Brokers"]
    UKR{{"🏴 Ukraine"}}
    WRL["World"]
    MIC["Microsoft"]
    USA --- MIC
    USA --- NSA
    ETB --> MIC
    MIM --> MIC
    NSA -.- MIC
    SHD --x NSA
    NSA --- ETB
    SHD --> ETB
    ETB --> SND
    MIM --> SND
    RUS --- SND
    RUS --- SHD
    SND --> NOT
    NOT --x UKR
    UKR --> WRL
    NOT --> MIC
```
