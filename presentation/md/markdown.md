# NotPetya <!-- .element: title="NotPetya" -->

<!-- .slide: data-background-image="./md/notpetya.svg" -->
<!-- .slide: data-background-transition="fade" -->
<!-- .slide: class="glitch" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.5" -->

---

<!-- .slide: data-auto-animate -->

## Contexte

|||

<!-- .slide: data-auto-animate -->

## Contexte

> "It's clear where the world is going. We're entering a world where every
> thermostat, every electrical heater, every air conditioner, every power plant,
> every medical device, every hospital, every traffic light, every automobile
> will be connected to the Internet. Think about what it will mean for the world
> when those devices are the subject of attack." Then he made his pitch. "The
> world needs a new, digital Geneva Convention."

<!-- .element: style="width: 90%; font-size: .75em;" -->

<!-- .slide: author="Greenberg 2019" -->

|||

<!-- .slide: data-auto-animate -->

## Contexte

-   Forme : Ransomware
-   Cible principale : Ukraine
-   Transmission : faille EternalBlue
-   Commanditaire : Russie ?

<img src="./md/bootscreen.png" width=500 style="margin-bottom: -0.5em;">

<!-- .slide: author="Sood et Hurley 2017" -->

---

## Analyse Situationelle

**󱁉** <!-- .element: style="font-size: 2em;" -->

|||

<pre class="mermaid">
flowchart TB
    NSA>"NSA"]
    USA{{"&nbsp États Unis"}}
    ETB(["󰚌 EternalBlue"])
    MIM(["󰚌 Mimikatz"])
    NOT(["󰚌 NotPetya"])
    RUS{{"&nbsp Russie"}}
    SND>"Sandworm"]
    SHD>"Shadow Brokers"]
    UKR{{"&nbsp Ukraine"}}
    WRL["Monde"]
    MIC["Microsoft"]
    USA --- MIC
    USA --- NSA
    ETB --> MIC
    MIM --> MIC
    NSA -.- MIC
    SHD --x NSA
    NSA --- ETB
    SHD --> ETB
    ETB --> SND
    MIM --> SND
    RUS --- SND
    RUS --- SHD
    SND --> NOT
    NOT --x UKR
    UKR --> WRL
    NOT --> MIC
</pre>

---

## Analyse Géopolitique

**󰇧** <!-- .element: style="font-size: 2em;" -->

|||

### Analyse Géopolitique

-   Difficultés d'attribution mais fortes suspicions de la Russie
-   Liens avec d'autres virus russes connus précédemment
-   Accusations formelles des États Unis et de leurs alliés

---

## Analyse Économique

**** <!-- .element: style="font-size: 2em;" -->

|||

<!-- .slide: data-auto-animate -->

<div class="r-stack">
    <div style="border-radius: .5em; background-color: hsla(240, 64%, 5%, 80%); padding: .5em;" data-id="box">
        <h3 style="color: transparent;" data-id="nottext">Analyse Économique</h3>
    </div>
    <h3 style="margin-top: .5em;" data-id="text">Analyse Économique</h3>
</div>

<!-- .slide: data-background-image="./md/maersk.jpg" -->
<!-- .slide: data-background-size="cover" -->
<!-- .slide: data-background-position="100% 100%" -->

|||

<!-- .slide: data-auto-animate -->

<div class="r-stack">
    <div style="border-radius: .5em; background-color: hsla(240, 64%, 5%, 80%); padding: .5em;" data-id="box">
        <h3 style="color: transparent;" data-id="nottext">Analyse Économique</h3>
        <ul class="fragment">
            <li>
                Ukraine, Russie, États-Unis...
            </li>
            <li>
                Maersk
            </li>
            <li>
                10 <em>milliards</em> de dollars
            </li>
            <li>
                4% du commerce <em>mondial</em>
            </li>
        </ul>
    </div>
    <h3 style="margin-top: .5em;" data-id="text">Analyse Économique</h3>
</div>

<!-- .slide: author="Getty Images" -->
<!-- .slide: author-yshift="500" -->

<!-- .slide: data-background-image="./md/maersk.jpg" -->
<!-- .slide: data-background-size="cover" -->
<!-- .slide: data-background-position="100% 100%" -->

---

## Analyse Sociale

**** <!-- .element: style="font-size: 2em;" -->

|||

### Perception

-   NotPetya: Révélation des risques
-   Conscience accrue des dangers

|||

### Comportement

-   Meilleures pratiques de sécurité
-   Éducation en cybersécurité

|||

### Collaboration

-   Collaboration internationale
-   Interdépendance globale

|||

### Leçons

-   Vigilance continue
-   Amélioration des défenses

---

## Analyse Technique

**** <!-- .element: style="font-size: 2em;" -->

|||

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-id="1" -->
<!-- .slide: data-autoslide="500" -->

### Fonctionnement 

<!-- .slide: data-background-image="./md/win.png" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.1" -->

|||

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-id="1" -->

### Fonctionnement 

<!-- .slide: data-background-image="./md/win.png" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.1" -->

<ul>
    <li class="fragment">
        MeDoc $\to$ attaque via <em>réseau</em>
    </li>
    <li class="fragment">
        Escalade de <em>privilèges</em>
    </li>
    <li class="fragment">
        Vol de <em>logins</em>
        <ul>
            <li class="fragment">
                Mimikatz
            </li>
            <li class="fragment">
                EternalBlue
            </li>
        </ul>
    </li>
    <li class="fragment">
        Propagation
    </li>
    <li class="fragment">
        MBR, MFT, fichiers
    </li>
    <li class="fragment">
        Redémarrage et rançon
    </li>
</ul>

<!-- .slide: author="Sood et Hurley 2017" -->

|||

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-id="1" -->

### Fonctionnement 

<!-- .slide: data-background-image="./md/win.png" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.1" -->

<pre class="mermaid">
mindmap
    root(())
        CO(Connectivité)
        OP(Opérabilité)
        FI(Fichiers)
</pre>

|||

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-id="2" -->
<!-- .slide: data-autoslide="500" -->

### Mimikatz 󰄛

<!-- .slide: data-background-image="./md/mimi.svg" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.1" -->

|||

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-id="2" -->

### Mimikatz 󰄛

<ul>
    <li class="fragment">
        Créé pour de la <em>recherche</em>
    </li>
    <li class="fragment">
        Logiciel <em>libre</em>
    </li>
    <li class="fragment">
        Vulnérabilité signalée <em>avant</em> publication
    </li>
    <li class="fragment">
        Microsoft décide de ce qui est patché $\to$ "prosumer"
    </li>
    <li class="fragment">
        Est-il éthique de publier des virus ?
    </li>
</ul>

<!-- .slide: author="Greenberg 2017" -->

<!-- .slide: data-background-image="./md/mimi.svg" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.1" -->

|||

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-id="3" -->
<!-- .slide: data-autoslide="500" -->

### EternalBlue 󰒍

<!-- .slide: data-background-image="./md/nsa.png" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.1" -->

|||

<!-- .slide: data-auto-animate -->
<!-- .slide: data-auto-animate-id="3" -->

### EternalBlue 󰒍

<!-- .slide: data-background-image="./md/nsa.png" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.1" -->

<ul>
    <li class="fragment">
        NSA "hacker tool"
    </li>
    <li class="fragment">
        Publié par "The Shadow Brokers"
    </li>
    <li class="fragment">
        Exploite SMB
    </li>
    <li class="fragment">
        Ancienne faille patchée <em>au bon moment</em>
    </li>
    <li class="fragment">
        Microsoft ignore $\leftrightarrow$ NSA dissumle
    </li>
    <li class="fragment">
        Microsoft sait et maintient $\leftrightarrow$ NSA utilise
    </li>
</ul>

<!-- .slide: author="Nakashima 2017" -->

---

<!-- .slide: data-auto-animate -->

## Conclusion

|||

<!-- .slide: data-auto-animate -->

## Conclusion

<ul>
    <li class="fragment">
        Prise de conscience
    </li>
    <li class="fragment">
        Dégâts plus que matériels
    </li>
    <li class="fragment">
        Collaborations et tensions
    </li>
    <li class="fragment">
        Légitimité des virus
    </li>
    <li class="fragment">
        État d'internet
    </li>
</ul>

|||

<!-- .slide: data-auto-animate -->

## Conclusion

> Life went very fast from 'What's new on Facebook ?' to 'Do I have enough money
> to buy food for tomorrow ?'

<!-- .slide: author="Greenberg 2019" -->

---

# Merci ! <!-- .element: title="Merci !" -->

<ul class="fragment" style="margin-top: 6rem; font-size: .5em; padding: .5em .5em .5em 2em; border-radius: 1em; background: linear-gradient(135deg, rgba(255, 0, 255, .1), rgba(0, 255, 255, .1)); backdrop-filter: blur(.5em);">
    <li>
        Greenberg, Andy. 2019. <em>Sandworm : A New Era of Cyberwar and the Hunt
        for the Kremlin's Most Dangerous Hackers.</em>
    </li>
    <li>
        Sood, Karan et Shaun Hurley. 2017. "NotPetya Technical Analysis - A
        Triple Threat : File Encryption, MFT Encryption, Credential Theft".
        <a
            href="https://www.crowdstrike.com/blog/petrwrap-ransomware-technical-analysis-triple-threat-file-encryption-mft-encryption-credential-theft/"
        >
            https://www.crowdstrike.com/blog/petrwrap-ransomware-technical-analysis-triple-threat-file-encryption-mft-encryption-credential-theft/
        </a>.
    </li>
    <li>
        Nakashima, Ellen et Craig Timberg. 2017. "NSA officials worried about
        the day its potent hacking tool would get loose. Then it did." <em>The
        Washington Post</em>.
        <a
            href="https://www.washingtonpost.com/business/technology/nsa-officials-worried-about-the-day-its-potent-hacking-tool-would- get- loose- then- it- did/2017/05/16/50670b16- 3978- 11e7- a058-ddbb23c75d82_story.html"
        >
            https://www.washingtonpost.com/business/technology/nsa-officials-worried-about-the-day-its-potent-hacking-tool-would- get- loose- then- it- did/2017/05/16/50670b16- 3978- 11e7- a058-ddbb23c75d82_story.html
        </a>.
    </li>
</ul>

<!-- .slide: data-background-image="./md/notpetya.svg" -->
<!-- .slide: data-background-title="hey" -->
<!-- .slide: class="glitch" -->
<!-- .slide: data-background-transition="fade" -->
<!-- .slide: data-background-size="auto 100%" -->
<!-- .slide: data-background-position="100% 100%" -->
<!-- .slide: data-background-opacity="0.5" -->
