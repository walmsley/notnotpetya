\documentclass[11pt, hidelinks, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[bottom]{footmisc}
\usepackage[margin=2cm]{geometry}

\usepackage{tgadventor}
\renewcommand*\familydefault{\sfdefault}
\usepackage[T1]{fontenc}

\usepackage{graphicx}
\usepackage{svg}
\usepackage{hyperref}
\usepackage[thmmarks, framed]{ntheorem}
\usepackage[french]{babel}
\usepackage{plex-mono}
\usepackage{comment}

\usepackage{titling}
\setlength{\droptitle}{-2.5cm}
\usepackage{titlesec}
\titlespacing*{\section}{0pt}{1\baselineskip}{0.25\baselineskip}

\usepackage{float}

\usepackage[style=french]{csquotes}
\usepackage[
    authordate,
    hypername,
    backend=biber,
]{biblatex-chicago}
\addbibresource{technical.bib}
\urlstyle{tt}

\usepackage{authblk}

\setcounter{secnumdepth}{0}
\title{NotPetya}

\renewcommand\Authands{ et }
\author[]{El\'ias Hauksson}
\author[]{Ismail Hassoun}
\author[]{Theobald A Walsh De Serrant}
\author[]{Eliot Gindrat}
\author[]{Kalan Walmsley}
\affil[]{Groupe 3, Science, Technologie et Société, EPFL}

\date{\the\year{}}

\begin{document}

\renewcommand{\mkbibnamefamily}[1]{\textsc{#1}}

\maketitle

\setlength{\abovecaptionskip}{-5pt}
\vspace{-.5cm}
\begin{figure}[H]
    \begin{center}
        \includesvg[width=.55\linewidth]{notpetya.svg}
    \end{center}
    \caption{Cartographie des relations entre acteurs}
\end{figure}

NotPetya est le virus qui fût l'arme principale dans ce que les experts
considèrent être le premier acte de cyberguerre au monde. Il porte ressemblance
à un ransomware, programme malveillant qui crypte les données d'un appareil,
les prenant en otage jusqu'à ce qu'un paiement soit effectué. Le virus toucha
le monde en juin 2017, dans une cyberattaque dont 80\% des infections se
trouvèrent en Ukraine \parencite{medoc}, le point zéro. Dans un premier temps
confondu avec le malware Petya, les experts notèrent vite qu'il s'agissait
d'une nouvelle menace et renommèrent le virus NotPetya dans le but de le
différencier.

La composante principale du virus est un outil de hackage pour Windows nommé
EternalBlue, conçu par la NSA et plus tard divulgué par le collectif d'hackers
"The Shadow Brokers". Même si la faille EternalBlue était, déjà au moment de
l'attaque, patchée par Microsoft, nombre d'entreprises n'avaient pas réalisé la
mise à jour. Malgré le message de rançon affiché sur chaque ordinateur lors de
l'infection, NotPetya n'est pas considéré comme ayant eu pour but d'extorquer
de l'argent mais uniquement de détruire les données de l'appareil affecté. En
effet, aucun utilisateur affecté n'a pu récupérer ses données. L'adresse
communiquée par les hackers pour le versement était désactivée et des analyses
subséquentes ont montré que les clés de cryptage étaient aléatoires et
irrécupérables.

Les dégâts de NotPetya se chiffrent en milliards de dollars
\parencite{untold-notpetya}. Des entreprises des secteurs tels que la finance,
l'énergie, la santé et la logistique furent touchées à travers le monde. La
cyberattaque ajouta des tensions aux relations entre l'Occident et la Russie,
non seulement à cause des dégâts d'ampleur causés en Ukraine mais aussi de ceux
causés à l'échelle mondiale. Le supposé usage par la Russie d'un virus
Américain soulève questions et débats. Aujourd'hui, on attribue cette
cyberattaque aux services de renseignements russes, plus particulièrement une
unité cyber nommée "Sandworm".

\section{Analyse}

L'origine de l'attaque de juin était le serveur de mise à jour du logiciel de
comptabilité Ukrainien MeDoc. Le serveur avait été infiltré et distribuait à
présent une mise à jour malveillante contenant NotPetya
\parencite{darknetdiaries}. Après infection via le réseau, NotPetya scrute
l'activité de certains antivirus et en module son attaque. NotPetya suit
l'évolution de ses privilèges dans le système -- les opérations qu'il est
capable d'effectuer. Une partie cruciale de l'escalade de privilèges est le
module "vol d'identité", qui utilise un virus nommé Mimikatz. NotPetya ne vole
pas uniquement l'identité d'un seul utilisateur, mais celle de tous les
utilisateurs connectés depuis le dernier redémarrage. Le virus infecte toute
autre machine possible avec les identités obtenues.

Finalement, le MBR (Master Boot Record) est manipulé, suivi par le MFT (Master
File Table), puis les fichiers des utilisateurs sont cryptés avant que la
machine ne redémarre pour afficher le message de rançon
\parencite{crowdstrike}. Les deux composantes les plus caractéristiques d'un
virus sont son mode d'infection et son exécution. NotPetya, grâce à
EternalBlue, exploite SMB, un protocole crucial pour la connectivité et la
communication. Le virus s'attaque non seulement à la machine, mais aux fichiers
des utilisateurs. Les vulnérabilités exploitées se cachent dans les éléments
les plus "humains" de la machine: connectivité, opérabilité et fichiers. En
effet, notre infrastructure numérique est bâtie autour de la communication, la
facilité d'emploi, le partage et stockage de données. La plupart des
utilisateurs désirent des outils pour communiquer et partager, faisant
abstraction des \texttt{1} et \texttt{0} derrière le voile. NotPetya lève ce
voile, profitant de vulnérabilités engendrées par ces fonctionnalités
fondamentales.

\paragraph{Mimikatz}
L'usage de l'outil de hackage Mimikatz suscite un débat autour du logiciel
libre. En effet, Mimikatz est un outil de hackage libre
(\href{https://creativecommons.org/licenses/by/4.0/}{CC BY 4.0}) créé par un
programmeur, Benjamin Delpy, à des fins de recherche \parencite{mimikatz}. Ce
logiciel n'est pas apparu sans raison. Delpy avait signalé à Microsoft la
vulnérabilité exploitée plusieurs mois avant de publier Mimikatz, d'abord en
source fermée, puis sous forme libre
\parencite{how-mimikatz-became-go-to-hacker-tool}. Il est intéressant
d'observer la relation entre un utilisateur et son système d'exploitation. Pour
Windows, Microsoft peut et doit choisir quelles vulnérabilités sont traitées.
En effet, la fermeture du code derrière Windows implique qu'eux-seuls sont
responsables des failles dans leur système. Ceci contraste avec des systèmes
libres tels Linux et BSD, où la position de l'utilisateur tend plutôt vers
celle du "prosumer". C'est-à-dire que chacun peut découvrir, signaler et même
réparer une faille.

Mimikatz a été créé à cause de la fermeture du code derrière Windows, mais
est-il éthique de publier un logiciel de hackage, même sous forme libre, pour
attirer de l'attention sur une vulnérabilité ? Delpy argumente que si ce
n'était pas Mimikatz, ce serait un autre et qu'il est mieux qu'il soit ouvert
au publique. Pourtant, Mimikatz a été utilisé dans certaines des pires
cyberattaques au monde: Carbanak, DigiNotar, BadRabbit et bien sûr NotPetya.
Est-ce que les dégâts sont la faute du créateur de l'outil, du hacker, de
l'utilisateur, du système d'exploitation ? Est-ce que tout code devrait pouvoir
être publié ? Ce n'est que du code finalement ; du texte dans un fichier.

Il est avantageux pour Microsoft de garder la source de Windows fermée.
Développer et vendre un produit qui fait tourner 70\% des ordinateurs de bureau
du monde \parencite{win-stat} est colossal en termes de part de marché. Mais
ceci pose une multitude de questions qui impliquent la sécurité et le
fonctionnement de notre société. Que se passe-t-il si Microsoft arrête de
développer Windows, ou cesse d'exister ? Microsoft a accès aux systèmes
informatiques du monde: utilisateurs, entreprises, gouvernements et armées --
devrait-ce être possible pour une seule entreprise d'avoir autant de pouvoir ?
Est-ce que Microsoft est "too big to fail" pour la planète entière ?

\paragraph{EternalBlue}
La plus grande composante de NotPetya est EternalBlue. EternalBlue est un virus
développé par la NSA capable d'infiltrer un système informatique Windows.
L'attaque NotPetya s'est déroulée le 27 juin 2017, faisant usage d'EternalBlue.
Pourtant, Microsoft avait patché la vulnérabilité le 14 mars de la même année
\parencites{eternalblue-cve}{eternalblue-patch}{engadget}, exactement un mois
avant le 14 avril, quand le virus fut divulgué par les "Shadow Brokers"
\parencite{shadowbrokers}. Cette chronologie d'événements est intéressante, car
la vulnérabilité dans Windows existait depuis au moins Windows XP (2001), mais
n'a que été réparée très proche de la publication du virus. De plus, la NSA
connaissait cette vulnérabilité depuis plus de cinq ans quand Microsoft l'a
patché \parencite{five-years}. Puisqu'il est véridique que la NSA communique
avec Microsoft, imaginer qu'une conversation a eu lieu entre ces deux entités
au sujet d'EternalBlue n'est pas inconcevable. Nous voyons dans ce cas deux
situations apparaître.

Premièrement, Microsoft aurait pu ignorer l'existence de cette vulnérabilité
dans Windows. Si tel était le cas, pourquoi est-ce qu'en plus de cinq ans la
NSA, organisation américaine, n'aurait pas prévenu Microsoft, entreprise
américaine, qu'ils avaient découvert une faille critique dans leur système ?
Est-ce qu'en constatant qu'ils avaient été infiltrés, la NSA aurait prévenu
Microsoft qu'une vulnérabilité exploitable était désormais libre et pouvait à
tout moment devenir publique ?

Deuxièmement, Microsoft aurait pu connaître cette vulnérabilité, mais attendait
de traiter du problème. Microsoft ne s'occupe que des vulnérabilités
considérées signifiantes, mais EternalBlue est clairement une vulnérabilité
critique. Il est donc possible que dans ce scénario, Microsoft ait pris
connaissance de la vulnérabilité, soit indépendamment, soit gr\^ace à la NSA,
et l'ait intentionnellement négligée.

Pour résumer, il est possible que la NSA collecte des vulnérabilités dans
Windows et les cachent de Microsoft, pratique qui leur a été reprochée dans le
passé \parencite{oday-hoarding}. Des vulnérabilités pourraient également être
intentionnellement maintenues dans Windows par Microsoft afin que la NSA ait
accès aux systèmes de ce type.

\paragraph{Coûts de NotPetya}
D'un point de vue économique, NotPetya a causé une catastrophe en Ukraine.
Plusieurs grandes entreprises ukrainiennes ont signalé des dommages importants
-- des systèmes furent compromis à la banque centrale de l'Ukraine, à la
téléphonie d'état, au métro municipal, à l'aéroport de Boryspil à Kyiv, etc.
\parencite{internatinal-companies-affected}. Le virus n'a pas cessé aux
frontières ukrainiennes. Notamment, la société pharmaceutique Merck aux
États-Unis et la compagnie pétrolière Rosneft en Russie furent affectées.

Mais l'entreprise qui a subi les dommages les plus importants est Maersk.
Maersk est un titan mondial du transport maritime, responsable de 76 ports dans
le monde entier, de plus de 800 navires et d'environ un cinquième du commerce
mondial \parencite{maersk-costs}. L'entreprise entière a été mise à genoux par
NotPetya sans être, pourtant, une cible de l'attaque. L'attaque déclencha une
panique au siège de Maersk ; les systèmes d'entrée et les réseaux téléphoniques
rendus inutilisables de par la rapide propagation du virus. À la fin de la
journée, leurs réseaux étaient tellement corrompus que l'entreprise ferma.

En quelques heures, 10\% des ordinateurs en Ukraine furent détruits
\parencite{econ-report}, ce qui affecta hôpitaux, banques, métros, aéroports et
systèmes de paiement par carte de crédit dans tout le pays. À l'échelle
mondiale, le coût total de la cyberattaque est estimé à environ 10 milliards de
dollars, dont au moins 300 millions pour Maersk seul. Selon les estimations de
Maersk, environ 20\% des produits qu'ils transportaient ont cessé de circuler
pendant l'attaque. Puisque l'entreprise gère environ 20\% de tout le commerce
international, NotPetya a essentiellement interrompu 4\% du commerce
international en infectant une seule entité.

\paragraph{Attribution et conscience internationale}
Bien que multiples éléments pointent vers la Russie en tant que responsable de
l'attaque, il est tout de même difficile de s'en assurer. Plusieurs entreprises
spécialisées ont remarqué des similarités notables entre NotPetya et d'autres
virus suspectés de provenir du Kremlin \parencite{geopolitical-monde}, mais la
nature des traces de ce genre de cyberattaque rend souvent ce type d'analyse
très complexe et moins concluant que nécessaire pour une attribution soutenue.
Les acteurs de la scène internationale mirent plus de six mois avant d'accuser
officiellement la Russie, le 15 février 2018. L'accusation fût portée par les
Five Eyes, alliance des services de renseignement Australiens, Canadiens,
Néo-Zélandais, Britanniques et Américains. Dans un communiqué, la Maison
Blanche lance <<~[NotPetya] fait partie des efforts actuels du Kremlin pour
déstabiliser l'Ukraine et montre encore plus clairement l'implication russe
dans ce conflit. C'était aussi une cyberattaque dangereuse et sans discernement
qui appelle des conséquences internationales.~>>. La Russie démenti toujours
toute accusation de responsabilité. Malgré cela, NotPetya a attiré l'attention
de la communauté internationale sur le rôle des hackers basés en Russie
soutenus par le Kremlin.

NotPetya était une alerte sur notre importante dépendance à notre
infrastructure numérique. L'attaque a perturbé des systèmes critiques à
l'échelle mondiale. Elle était plus qu'un signal d'alarme, elle a remodelé
notre compréhension collective de la cybersécurité. Avant NotPetya, beaucoup
considéraient les menaces cyber comme des préoccupations secondaires. Le chaos
causé par cette attaque suscita une prise de conscience. En plus des
entreprises qui repensaient leurs stratégies de sécurité, les individus aussi
reconnaissaient leurs rôles dans l'écosystème numérique.

L'attaque a également remis en question notre confiance aux systèmes numériques
et à ceux responsables de leur protection et maintenance
\parencite{responsibility-in-cyber-warfare}. Ces préoccupations ne se
confinaient pas à la communauté technologique. Elles déclenchaient des
conversations dans les foyers, les lieux de travail et les couloirs
gouvernementaux sur les manières de mieux protéger leurs espaces numériques.
Dans le sillage de NotPetya, nous avons constaté une augmentation d'efforts
pour renforcer la cybersécurité. Plus de ressources se faisaient allouées à
éduquer le public sur les risques informatiques. Les écoles, les entreprises et
les agences gouvernementales ont commencé à mettre en avant l'éducation cyber,
reconnaissant que la sensibilisation est la première ligne de défense dans
l'ère numérique.

NotPetya, révélant la nature interconnectée de notre infrastructure numérique,
a également stimulé des débats sur le droit international en cybersécurité.
Devrait-il être permis aux nations de posséder des virus et les utiliser en
tant qu'arme ? Qu'en est-il du supposé usage d'EternalBlue (NSA) par la Russie
? Ceci soulève également des controverses concernant la neutralité d'Internet.
Est-ce qu'Internet est toujours neutre, même si la plupart appartient à
quelques entreprises comme Microsoft, Cloudflare, Google, provenant d'une
poignée de pays et basées sur des principes de code fermé ? Est-ce qu'Internet
est simplement un nouveau vecteur d'attaque ou champ de bataille ? Comment
équilibrer la sécurité globale avec les principes de liberté en ligne ?
L'attaque a souligné l'urgence d'une coopération et d'une responsabilité
partagées pour affronter ces défis informatiques, locale et internationale --
consolider les défenses informatiques, tout en respectant les libertés
individuelles.

Dans les années suivant NotPetya, nous avons observé une évolution dans
l'industrie de la cybersécurité \parencite{case-study-the-notpetya-campaign}.
Des leçons de l'attaque découla un intérêt accru pour le développement de
technologies de sécurité plus résilientes et avancées. La cybersécurité s'est
ancrée comme aspect essentiel de notre vie quotidienne.

\section{Conclusion}

La cyberattaque de NotPetya souligne la fragilité de notre monde numérique
interconnecté et notre dépendance à celui-ci. L'idée qu'une seule cyberattaque
informatique puisse engendrer tant de dégâts, de coûts et de tensions au niveau
international semblait impossible avant NotPetya. Cet incident met en évidence
la nécessité d'une collaboration internationale dans la défense contre les
cybermenaces, soulignant le délicat équilibre entre progrès technologique et
sécurité. Il suscite également des controverses autour de la surveillance et de
la légitimité des virus tels que EternalBlue, Mimikatz, et NotPetya.

\noindent
Internet se promettait neutre, décentralisé et résilient. NotPetya prouve le
contraire.

\begin{flushleft}
    <<~Life went very fast from 'What's new on Facebook?' to 'Do I have enough
    money to buy food for tomorrow?'~>>
\end{flushleft}
\begin{flushright}
    --- \cite{sandworm}, \citetitle{sandworm}
\end{flushright}

\newpage
\printbibliography

\end{document}
