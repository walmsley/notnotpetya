\documentclass[12pt, hidelinks, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[bottom]{footmisc}
\usepackage[margin=2.5cm]{geometry}

\usepackage{tgadventor}
\renewcommand*\familydefault{\sfdefault}
\usepackage[T1]{fontenc}

\usepackage{graphicx}
\usepackage{physics}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{hyperref}
\usepackage{marvosym}
\usepackage{multicol}
\usepackage{centernot}
\usepackage[dvipsnames]{xcolor}
\usepackage[makeroom]{cancel}
\renewcommand*\CancelColor{\color{CarnationPink}}
\usepackage{framed}
\usepackage[thmmarks, framed]{ntheorem}

\usepackage{biblatex}
\addbibresource{technical.bib}


\title{NotPetya\\Technical Aspects}
\author{Kalan Walmsley}
\date{\today}

\begin{document}

\maketitle

\thispagestyle{empty}
\newpage
\tableofcontents
\newpage

\begin{center}
    \includegraphics[width=.75\linewidth]{bootscreen.png}
\end{center}

\section{What did the attack target}
NotPetya enters local networks via a supply chain attack on the
update mechanism of Ukraine's M.E.Doc tax management software.
Once infected by NotPetya, a series of malicious activities ensue, including the
following \cite{crowdstrike}

\begin{itemize}
    \item Dropped files
          \begin{itemize}
              \item Ransomware DLL
              \item PsExec utility : allows execution of processes on other
                    systems
              \item Credential theft module
          \end{itemize}
    \item Process hashes : check for
          \begin{itemize}
              \item Kaspersky AV
              \item Norton Security
              \item Symantec
          \end{itemize}
    \item Process privilege checks
          \begin{itemize}
              \item Keep track of the following privileges
                    \begin{itemize}
                        \item Can shutdown the system
                        \item Can debug or adjust memory for a process owned by
                              another account
                        \item Can assume identity of any user
                    \end{itemize}
              \item The credential theft module is only dropped if the malware
                    has certain privileges
              \item MBR overwrite and MFT encryption subroutine not run if
                    Kaspersky
              \item The EternalBlue subroutine is not executed if Norton or
                    Symantec
              \item The system is rebooted if it can't shutdown the system
          \end{itemize}
    \item Credential theft : use of Mimikatz
    \item Token impersonation : for all impersonatable users, executes SMB
          (Server Message Block) copy and remote execution as those users
    \item Malware propagation
          \begin{itemize}
              \item Network node enumeration
              \item SMB copy and remote execution
              \item SMB exploitation via EternalBlue
          \end{itemize}
    \item Attempts to write malware to admin\$ on remote target
    \item Remote execution of the malware
    \item MBR (Mater Boot Record) ransomware
          \begin{itemize}
              \item Physical drive manipulation
              \item MFT (Master File Table) encryption
              \item File encryption
          \end{itemize}
    \item System shutdown
    \item Anti-forensics
          \begin{itemize}
              \item Loads itself in memory and deletes itself from the disk
              \item Clear logs
          \end{itemize}
\end{itemize}

\section{Issues related to open source software}

\subsection{Mimikatz}

NotPetya uses a tool called Mimikatz \cite{mimikatz}, which was created for
research purposes by French programmer Benjamin Delpy. The following points were
reported in \cite{how-mimikatz-became-go-to-hacker-tool}

\begin{itemize}
    \item First released in May 2011 as closed source software, after notifying
          Microsoft of the vulnerability and them responding that the machines
          already had to be compromised
    \item In September 2011, the exploit was used in the DigiNotar hack
    \item After an attempted breach of his latop during a conference about
          Mimikatz, he released the source code, so people could learn from it
\end{itemize}

\subsection{EternalBlue}

\begin{itemize}
    \item The NotPetya attack took place on June 27th 2017 using EternalBlue,
          but Microsoft had patched the vulnerability on the 14th of March 2017
          \cite{eternalblue-patch} \cite{engadget}
    \item EternalBlue was released by The Shaddow Brokers on April 14 2017
          \cite{shadowbrokers}
\end{itemize}

\subsection{Text}

\subsubsection{How does it work ?}

Pour commencer à comprendre NotPetya, il nous faut comprendre un peu son
fonctionnement. Après avoir infecté une machine via le réseau, NotPetya analyse
les processus en cours pour détecter certains antivirus: Kaspersky, Norton et
Symantec. NotPetya suit l'évolution de ses privilèges dans le système (éteindre
la machine, modifier le processus d'un autre utilisateur, usurper l'identité
d'un autre utilisateur).

Une partie cruciale de l'escalade de privilèges est le module "vol d'identité",
qui utilise un virus nommé Mimikatz. NotPetya ne vol pas uniquement l'identité
d'un seul utilisateur, mais aussi celle de tous les autres utilisateurs qui se
sont connectés depuis le dernier redémarrage. Si NotPetya a accès à plus
d'utilisateurs ou tient des identités admin, le virus infecte toutes les autres
machines possibles.

Finalement, le MBR (Master Boot Record) est manipulé, puis le MFT (Master File
Table) et les fichiers des utilisateurs sont cryptés, avant que la machine
redémarre pour afficher le message de rançon.

Pendant tout ce temps, le virus est stocké dans la RAM et se supprime du
disque, tout en détruisant toutes traces dans le registres.

\subsubsection{Technical analysis}

Les deux composantes les plus caractéristiques d'un virus sont sont mode
d'infection et ce que le virus fait après l'infection. NotPetya exploite SMB, un
protocole crucial pour la connectivité et la communication
\emph{
    <<~ Le protocole SMB (Server Message Block) est un protocole de partage de
    fichiers réseau qui permet à des applications installées sur un ordinateur
    d'accéder en lecture et en écriture à des fichiers et de solliciter des
    services auprès de programmes serveur sur un réseau informatique. ~>>
}
et s'attaque non seulement à la machine, mais aux fichiers des utilisateurs. Ce
sont les éléments les plus "humains" de la machine: connectivité, opérabilité et
fichiers.

\subsubsection{Controverses}
Une multitude de controverses techniques orbitent autour de ce virus, mais nous
allons discuter des deux plus grandes, cachées au c\oe ur de cette histoire.

\paragraph{Mimikatz}
L'usage de l'outil de hacking Mimikatz suscite un débat autour du logiciel
libre. En effet, pour être bref, Mimikatz est un outil de hacking libre
(\href{https://creativecommons.org/licenses/by/4.0/}{CC BY 4.0 Deed}) qui a été
créé par une seule personne à des fins de recherche. Le fait que ce logiciel
soit libre indique que n'importe qui peut l'utiliser pour voler les "identités"
des utilisateurs d'un système Windows.

Ce logiciel n'est pas apparu sans raison par contre. Le développer, Benjamin
Delpy, avait signalé à Microsoft la vulnérabilité plusieurs mois avant de le
publier, d'abord en source fermée, puis publiquement.

Est-il éthique de publier des logiciels de hacking en public ? Delpy argumente
que si ce n'était pas Mimikatz, ce serait autre chose, et qu'il est mieux qu'il
soit ouvert au publique. Pourtant, Mimikatz a été utilisé dans certaines des
pires cyberattaques au monde: Carbanak, DigiNotar, BadRabbit, et bien sûr
NotPetya. Est-ce que les dégâts sont la faute du créateur de l'outil, ou de
l'utilisateur ? Ce n'est que du code finalement...

\paragraph{NSA et Microsoft}
Une autre composante, sans doute une des plus grandes, de NotPetya est
EternalBlue. EternalBlue est un virus développé par la NSA 
L'attaque NotPetya s'est déroulée le 27 juin 2017, faisant 

The NotPetya attack took place on June 27th 2017 using EternalBlue, but
Microsoft had patched the vulnerability on the 14th of March 2017 [4] [3]
• EternalBlue was released by The Shaddow Brokers on April 14 2017 [6]




\newpage
\printbibliography
\end{document}
