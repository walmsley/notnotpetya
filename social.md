# Social Aspect of the NotPetya Attack

## Content

The NotPetya cyberattack in 2017 wasn't just a technological disaster. It was a big reminder of our deep dependency on digital infrastructure. With damages in the billions, it disrupted critical systems globally. This attack was more than a wake-up call. It reshaped our collective understanding of cybersecurity's importance.
Before NotPetya, many viewed online risks as distant concerns. However, the widespread chaos caused by this attack brought a newfound awareness. It wasn't just businesses rethinking their security strategies. Everyday individuals began to recognize their role in this digital ecosystem. People became more vigilant about updating software, using stronger passwords, and understanding the basics of online safety.
The attack also caused a significant shift in how we trust digital systems and those responsible for safeguarding them. The vulnerability of such crucial infrastructure raised questions about our preparedness for digital threats. This concerns wasn't confined to just the tech community. It also sparked conversations in households, workplaces, and government halls about how to better protect our digital world.
In the wake of NotPetya, we saw a surge in efforts to bolster cybersecurity. There was a noticeable increase in resources dedicated to educating the public about cyber risks. Schools, businesses, and government agencies started to prioritize cyber education, recognizing that awareness is the first line of defense in this digital age.
Moreover, the incident caused a broader societal change. It highlighted the interconnected nature of our world and how a digital threat in one corner of the world can have global implications. Communities began to understand the importance of cooperation and shared responsibilty in tackling cyber challenges. This led to more collaborative efforts, both locally and internationally, to strengthen cyber defenses.
In the years following NotPetya, we've seen an evolution in the cybersecurity industry. Driven by the lessons of the attack, there's been an increased focus on developing more resilient and advanced security technologies. But also more importantly, there's been a shift in mindset. Cybersecurity is now seen as an essential aspect of our daily lives.

## Links
- https://www.researchgate.net/profile/Csaba-Krasznay/publication/353072644_Case_Study_The_NotPetya_Campaign/links/60e6bbe00fbf460db8ee783d/Case-Study-The-NotPetya-Campaign.pdf (last accessed on Nov 20)
- https://su-plus.strathmore.edu/server/api/core/bitstreams/3257327b-ea38-4f4b-895c-d77eabee10ff/content (last accessed on Nov 20)
